import turtle

def Cmain():
	###Part-1### 
	t=turtle.Turtle()
	screen=turtle.Screen()
	turtle.tracer(1,0)
	t.ht()
	t.penup()
	t.goto(5,-80)
	t.pendown()
	t.pencolor("#3fccf9")
	t.fillcolor("#3fccf9")
	t.width(3)
	t.begin_fill()
	t.circle(200)
	t.end_fill()
	t.penup()
	###Part-2###
	t.pencolor("#d4350f")
	t.fillcolor("#57ea1d")
	t.goto(-175,-51)
	t.width(7)
	t.pendown()
	t.begin_fill()
	t.goto(70,50)
	t.goto(70,130)
	t.goto(50,140)
	t.goto(30,120)
	t.goto(190,280)
	t.goto(-25,175)
	t.goto(-50,80)
	t.goto(-5,70)
	t.goto(20,90)
	t.goto(-175,-51)
	t.end_fill()
	
	screen.exitonclick()	
#########################
	
Cmain()
