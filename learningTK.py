#https://realpython.com/python-gui-tkinter/#building-your-first-python-gui-application-with-tkinter#
import tkinter as tk

window=tk.Tk()
greeting=tk.Label(text="Hello, Tkinter", fg="#34A2FE",
	bg="black",
	width=32,
	height=5,
	font=("Courier", 10))
greeting.pack()
button=tk.Button(text="Don't push me", width=25, height=5, bg="green", fg="yellow")
button.pack()
window.mainloop()


